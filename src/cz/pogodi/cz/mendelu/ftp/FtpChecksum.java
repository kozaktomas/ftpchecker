package cz.pogodi.cz.mendelu.ftp;

import cz.pogodi.cz.mendelu.hash.MD5;
import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tomáš Kozák
 * @version 1.0
 * @since 2015-1-17
 */
public class FtpChecksum {

    private final FtpDiscover ftpDiscover;

    private final FTPClient ftpClient;

    private MD5 md5;

    public FtpChecksum(FtpDiscover ftpDiscover) {
        this.ftpDiscover = ftpDiscover;
        this.ftpClient = ftpDiscover.getFTPClient();
        this.md5 = new MD5();
    }

    public String getChecksum() {

        File tempFile = new File("temp.tmp");
        ArrayList<String> files = ftpDiscover.getFileList();

        try {
            for (String fileName : files) {
                ftpClient.download(fileName, tempFile);

                byte[] bFile = new byte[(int) tempFile.length()];
                FileInputStream is = new FileInputStream("temp.tmp");
                is.read(bFile);
                this.md5.append(bFile);

                is.close();
            }

        } catch (IllegalStateException | IOException | FTPIllegalReplyException | FTPException | FTPDataTransferException | FTPAbortedException ex) {
            Logger.getLogger(FtpChecksum.class.getName()).log(Level.SEVERE, null, ex);
        }

        tempFile.delete();
        return this.md5.computeMD5();
    }

}
