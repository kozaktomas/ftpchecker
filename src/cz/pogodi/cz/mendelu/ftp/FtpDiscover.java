package cz.pogodi.cz.mendelu.ftp;

import cz.pogodi.cz.mendelu.Configuration;
import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;
import it.sauronsoftware.ftp4j.FTPListParseException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * @author Tomáš Kozák
 * @version 1.0
 * @since 2015-1-17
 */
public class FtpDiscover {

    FTPClient client;

    ArrayList<String> fileList = new ArrayList<>();

    private final Configuration config;

    public FtpDiscover(Configuration config) {
        this.client = new FTPClient();
        if (config.isSecured()) {
            this.setSecureClient();
        }

        String[] credit = config.getUrl().getUserInfo().split(":");

        try {
            this.client.connect(config.getUrl().getHost());
            this.client.login(credit[0], credit[1]);
        } catch (IllegalStateException | IOException | FTPIllegalReplyException | FTPException ex) {
            Logger.getLogger(FtpDiscover.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.config = config;

    }

    public ArrayList getFileList() {
        this.recursiveFinder("/");

        /*

         this.fileList.sort(new Comparator<String>(){

         @Override
         public int compare(String o1, String o2) {
         return 1;
         }
        
         });
         */
        return this.fileList;
    }

    public FTPClient getFTPClient() {
        return this.client;
    }

    /**
     * @see TYPE 1 - folder TYPE 0 - file
     */
    private void recursiveFinder(String absoluteDirectory) {
        FTPFile files[];

        try {
            this.client.changeDirectory(absoluteDirectory);
            files = client.list();
            for (FTPFile file : files) {

                if (config.isIgnored(file.getName())) {
                    continue;
                }

                if (1 == file.getType()) {
                    this.recursiveFinder(absoluteDirectory + file.getName() + "/");
                } else if (0 == file.getType()) {
                    this.fileList.add(absoluteDirectory + file.getName());
                }
            }

        } catch (IllegalStateException | IOException | FTPIllegalReplyException | FTPException | FTPDataTransferException | FTPAbortedException | FTPListParseException ex) {
            Logger.getLogger(FtpDiscover.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void setSecureClient() {
        TrustManager[] trustManager = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManager, new SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
        }
        SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
        this.client.setSSLSocketFactory(sslSocketFactory);
        this.client.setSecurity(FTPClient.SECURITY_FTPS);
    }

}
