package cz.pogodi.cz.mendelu;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Wini;
import java.net.URL;

/**
 * @author Tomáš Kozák
 * @version 1.0
 * @since 2015-1-17
 */
public class Configuration {

    private String[] ignoreMasks;

    private URL url;

    private String localDir;

    private boolean secured = false;

    public Configuration(String configPath) {
        File iniFile = new File(configPath);
        try {
            Wini ini = new Wini(iniFile);
            this.url = new URL(ini.get("mysite", "remote", String.class));
            this.localDir = ini.get("mysite", "local", String.class);
            String ignoreString = ini.get("mysite", "ignore", String.class);
            this.ignoreMasks = ignoreString.split(";");
            String secureString = ini.get("mysite", "secure", String.class);
            if (secureString.equals("true")) {
                this.secured = true;
            }
        } catch (IOException ex) {
            Logger.getLogger(FtpChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public URL getUrl() {
        return url;
    }

    public boolean isIgnored(String name) {
        for (String mask : this.ignoreMasks) {
            if (name.matches(mask)) {
                return true;
            }
        }
        return false;
    }

    public String getLocalDir() {
        return localDir;
    }

    public boolean isSecured() {
        return this.secured;
    }

}
