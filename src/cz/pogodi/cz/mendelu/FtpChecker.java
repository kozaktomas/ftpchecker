package cz.pogodi.cz.mendelu;

import cz.pogodi.cz.mendelu.directory.DirectoryChecksum;
import cz.pogodi.cz.mendelu.ftp.FtpDiscover;
import cz.pogodi.cz.mendelu.ftp.FtpChecksum;

/**
 * @author Tomáš Kozák
 * @version 1.0
 * @since 2015-1-17
 */
public class FtpChecker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Configuration config = new Configuration(args[0]);

        DirectoryChecksum dcs = new DirectoryChecksum(config);

        FtpDiscover ftp = new FtpDiscover(config);
        FtpChecksum cs = new FtpChecksum(ftp);

        System.out.println("");
        System.out.println("");
        if (cs.getChecksum().equals(dcs.getChecksum())) {
            System.out.println("Everything OK!");
            System.out.println("________   ____  __.\n"
                    + "\\_____  \\ |    |/ _|\n"
                    + " /   |   \\|      <  \n"
                    + "/    |    \\    |  \\ \n"
                    + "\\_______  /____|__ \\\n"
                    + "        \\/        \\/");
        } else {
            System.out.println("FAIL!");
            System.out.println("________________  .___.____     \n"
                    + "\\_   _____/  _  \\ |   |    |    \n"
                    + " |    __)/  /_\\  \\|   |    |    \n"
                    + " |     \\/    |    \\   |    |___ \n"
                    + " \\___  /\\____|__  /___|_______ \\\n"
                    + "     \\/         \\/            \\/");
        }
        System.out.println("");
    }

}
