package cz.pogodi.cz.mendelu.directory;

import cz.pogodi.cz.mendelu.Configuration;
import cz.pogodi.cz.mendelu.hash.MD5;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tomáš Kozák
 * @version 1.0
 * @since 2015-1-17
 */
public class DirectoryChecksum {

    private String directory;

    private MD5 md5;

    private Configuration config;

    public DirectoryChecksum(Configuration config) {
        this.directory = config.getLocalDir();
        md5 = new MD5();
        this.config = config;
    }

    public String getChecksum() {
        this.recursiveFinder(this.directory);
        return md5.computeMD5();
    }

    public void recursiveFinder(String path) {

        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) {
            return;
        }

        for (File f : list) {

            if (config.isIgnored(f.getName())) {
                continue;
            }

            if (f.isDirectory()) {
                this.recursiveFinder(f.getAbsolutePath());
            } else {

                byte[] bFile = new byte[(int) f.length()];
                FileInputStream is;
                try {

                    is = new FileInputStream(f.getPath());

                    is.read(bFile);

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(DirectoryChecksum.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DirectoryChecksum.class.getName()).log(Level.SEVERE, null, ex);
                }

                this.md5.append(bFile);
            }
        }
    }
}
