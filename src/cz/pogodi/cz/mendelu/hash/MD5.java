package cz.pogodi.cz.mendelu.hash;

/**
 * @author Tomas Kozik
 * @version 1.0
 * @since 2015-1-17
 */
public class MD5 {

    private static final int[] SHIFT_AMTS = {
        7, 12, 17, 22,
        5, 9, 14, 20,
        4, 11, 16, 23,
        6, 10, 15, 21
    };

    private static final int[] TABLE_T = new int[64];

    private int a = 0x67452301;
    private int b = (int) 0xEFCDAB89L;
    private int c = (int) 0x98BADCFEL;
    private int d = 0x10325476;

    int[] buffer = new int[16];

    public MD5() {
        for (int i = 0; i < 64; i++) {
            TABLE_T[i] = (int) (long) ((1L << 32) * Math.abs(Math.sin(i + 1)));
        }
    }

    public void append(byte[] message) {

        int length = message.length;
        int numBlocks = ((length + 8) >>> 6) + 1;

        int totalLen = numBlocks << 6;
        byte[] paddingBytes = new byte[totalLen - length];
        paddingBytes[0] = (byte) 0x80;

        long messageLenBits = (long) length << 3;
        for (int i = 0; i < 8; i++) {
            paddingBytes[paddingBytes.length - 8 + i] = (byte) messageLenBits;
            messageLenBits >>>= 8;
        }

        for (int i = 0; i < numBlocks; i++) {
            int index = i << 6;
            for (int j = 0; j < 64; j++, index++) {
                buffer[j >>> 2] = ((int) ((index < length) ? message[index] : paddingBytes[index - length]) << 24) | (buffer[j >>> 2] >>> 8);
            }
            int originalA = this.a;
            int originalB = this.b;
            int originalC = this.c;
            int originalD = this.d;
            for (int j = 0; j < 64; j++) {
                int div16 = j >>> 4;
                int f = 0;
                int bufferIndex = j;
                switch (div16) {
                    case 0:
                        f = (this.b & this.c) | (~this.b & this.d);
                        break;

                    case 1:
                        f = (this.b & this.d) | (this.c & ~this.d);
                        bufferIndex = (bufferIndex * 5 + 1) & 0x0F;
                        break;

                    case 2:
                        f = this.b ^ this.c ^ this.d;
                        bufferIndex = (bufferIndex * 3 + 5) & 0x0F;
                        break;

                    case 3:
                        f = this.c ^ (this.b | ~this.d);
                        bufferIndex = (bufferIndex * 7) & 0x0F;
                        break;
                }
                int temp = this.b + Integer.rotateLeft(this.a + f + buffer[bufferIndex] + TABLE_T[j], SHIFT_AMTS[(div16 << 2) | (j & 3)]);
                this.a = this.d;
                this.d = this.c;
                this.c = this.b;
                this.b = temp;
            }

            this.a += originalA;
            this.b += originalB;
            this.c += originalC;
            this.d += originalD;
        }
    }

    public String computeMD5() {
        byte[] md5 = new byte[16];
        int count = 0;
        for (int i = 0; i < 4; i++) {
            int n = (i == 0) ? this.a : ((i == 1) ? this.b : ((i == 2) ? this.c : this.d));
            for (int j = 0; j < 4; j++) {
                md5[count++] = (byte) n;
                n >>>= 8;
            }
        }
        return this.toHexString(md5);
    }

    private String toHexString(byte[] b) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < b.length; i++) {
            sb.append(String.format("%02X", b[i] & 0xFF));
        }
        return sb.toString();
    }

}
