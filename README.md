FTP Checker
===========
This repository is being developed and it's highly unstable.

Description
-----------
The application check uploaded files to FTP[S] server.

Requirements
------------
FTP Checker requires JAVA version 7. Dependencies: ftp4j(1.7.2) and ini4j(0.5.2)

Run
---
```ini
java -jar path_to_ini_file
```


Example configuration
---------------

```ini
[mysite]
; remote FTP server
remote = ftp://username:password@hostname.com/your/directory

; local files
local = C:\path\to\your\local\code

; files and directories to ignore - mask for regexp - semicolon separated
ignore = temp;.*\.idea;.svn;log

; secure [FTPS]
secure = true
```